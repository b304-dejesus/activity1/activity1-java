package com.zuitt.wdc0043;

import java.util.Scanner;

public class Activity {

    public static void main(String[] args) {

        Scanner MyObj = new Scanner(System.in);

        System.out.println("Enter First name: ");
        String FirstName = MyObj.nextLine();

        System.out.println("Enter Last name: ");
        String LastName = MyObj.nextLine();

        System.out.println("Enter First Subject: ");
        Double FirstSubject = MyObj.nextDouble();

        System.out.println("Enter Second Subject: ");
        Double SecondSubject = MyObj.nextDouble();

        System.out.println("Enter Third Subject: ");
        Double ThirdSubject = MyObj.nextDouble();



        System.out.println("First Name:");
        System.out.println(FirstName);

        System.out.println("Last Name:");
        System.out.println(LastName);
        System.out.println();

        System.out.println("First Subject Grade:");
        System.out.println(FirstSubject);
        System.out.println();

        System.out.println("Second Subject Grade:");
        System.out.println(SecondSubject);
        System.out.println();

        System.out.println("Third Subject Grade:");
        System.out.println(ThirdSubject);
        System.out.println();

        System.out.println("Good day," + FirstName +" "+ LastName + ".");
        System.out.println("Your grade average is: " + ((FirstSubject + SecondSubject+ ThirdSubject)/3));




    }

}
