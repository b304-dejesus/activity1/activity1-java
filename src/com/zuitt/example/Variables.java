package com.zuitt.example;

public class Variables {

    public static void main(String[] args){
        //Declaration of identifier

        int age;
        char middleName;
        //variable initialization
        int x;
        int y = 0;

        x = 1;

        System.out.println("The value of y " + x);


        int wholeNumber =100;
        System.out.println(wholeNumber);

        long popWorld = 12345667886543454l;
        System.out.println(popWorld);

        float piFloat= 3.1416f;
        System.out.println(piFloat);

        double piDouble = 3.141614144432345;
        System.out.println(piDouble);

        char letter = 'a';
        System.out.println(letter);

        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        //nonPrimitive data Type
        //String
        String username = "JSMith";
        System.out.println(username);

        int stringLength = username.length();
        System.out.println(stringLength);





    }


}
